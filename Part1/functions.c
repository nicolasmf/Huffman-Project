#include <stdio.h>
#include <stdlib.h>
#include <string.h>

unsigned int2bin(unsigned k) {
    if (k == 0) return 0;
    if (k == 1) return 1;
    return (k % 2) + 10 * int2bin(k / 2);
}

// We have to add a certain amount of '0' at the beginning of the buffer if there are less than 8 caracters in it 
char* buffer2fullbinary(char* buffer){
    if (strlen(buffer) == 8)
        return buffer;
    else{
        char *binary = malloc(sizeof(char) * 10);
        for (int i = 0; i < 8 - strlen(buffer); i++){
            binary[i] = '0';
        }
        strcat(binary, buffer);
        return binary;
    }
}

int fileCharCount(FILE* file){
    int counter = 0;
    for (char c = fgetc(file); c != EOF; c = fgetc(file)) 
        counter ++;
    return counter;
}