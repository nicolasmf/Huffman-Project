#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "protoypes.h"

int main(){
    char buffer[8];
    FILE *input = fopen("Alice.txt", "r+");
    FILE *output = fopen("Output.txt", "w+");

    if(input != NULL){
        do{
            char actualChar = fgetc(input);
            //printf("%c\n", actualChar);
            unsigned charInt = int2bin(actualChar);
            
            if ((int)charInt > 0){
                //printf("%d\n", charInt);
                sprintf(buffer, "%d", charInt);
                char* fullBin = buffer2fullbinary(buffer);
                //printf("%s ", fullBin);
                fputs(fullBin, output);
                free(fullBin);
            }
            
        }while(!feof(input));
        printf("[*]Done.\n");
    }
    else{
        printf("[!]File not found.");
        return 0;
    }

    rewind(input);
    rewind(output);
    printf("Input : %d characters\nOutput : %d characters\n",fileCharCount(input), fileCharCount(output));

    fclose(input);
    fclose(output);
    return 0;
}