#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include "prototypes.h"

Node* occurence2sll(FILE *file){
    int count[256] = { 0 };
    int c;
    Node* head = (Node*)malloc(sizeof(Node));

    do {
        c = fgetc(file);
        count[c]++;
    }while (!feof(file));
    
    Node* node = (Node*)malloc(sizeof(Node));
    node = head;
    node->letter = '\n';
    node->occurence = count[0];
    node->next = NULL;
    
    for(int i=0; i<256; i++) {
        if(count[i] > 0){
            /*if(i == '\n')
                printf("char '\\n': %d times\n", count[i]);
            else    
                printf("char %c: %d times\n", i, count[i]);*/
            Node* new = (Node*)malloc(sizeof(Node));
            node->next = new;
            node = new;
            node->letter = i;
            node->occurence = count[i];
            node->next = NULL;
        }
    }
    fclose(file);

    node = head;

    return node;
}

void print_sll(Node *node){
    if(node == NULL) return;
    
    if (node->occurence != 0)
        printf("%c : %d\n", node->letter, node->occurence);
    print_sll(node->next);
}


char* get_binary_string(Tree *tree){
    if(tree == NULL) return NULL;
}

void create_dico(Tree *tree){
    FILE* dico = fopen("dico.txt","w+");
    fprintf(dico, "%c : %s", tree->letter, tree->binary); // To change
    fclose(dico);
}