typedef struct Node{
    char letter;
    int occurence;
    struct Node* next;
}Node;

Node* occurence2sll(FILE *file); // Create a linked list with letters and their occurences
void print_sll(Node *node); // Print a Linked list 


typedef struct Tree{
    char letter;
    int occurence;
    char* binary;
    struct Tree *left, *right;
}Tree;

/* Functions to change when I have Laurent's code*/
char* get_binary_string(Tree *tree);
void create_dico(); 