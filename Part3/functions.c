#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include "prototypes.h"

Node* addOcc(char c, Node* linked_list){
    
    if (linked_list->letter == '$'){ //We are in the first node of the list
        linked_list->letter = c;
        linked_list->occurence = 1;
        return linked_list;

    }else{
        if (linked_list->letter == c){
            linked_list->occurence++;

        }else{
            if (linked_list->next == NULL){
                Node* newNode = (Node*)malloc(sizeof(Node));
                linked_list->next = newNode;
                newNode->letter = c;
                newNode->occurence = 1;
                newNode->next = NULL;
                return linked_list;

            }else{
                addOcc(c, linked_list->next);
            }
        }
    }
    
    
}

void search(FILE* file, Node* linked_list){
    do {
        char c = fgetc(file);
        if (c != EOF)
            addOcc(c,linked_list);
    }while (!feof(file));
}

void print_sll(Node *linked_list){

    Node* temp_node = linked_list;

    if(temp_node == NULL) return;

    if(temp_node->next == NULL){
        printf("%c:%d\n", temp_node->letter, temp_node->occurence);
    }else{
        printf("%c:%d -> ", temp_node->letter, temp_node->occurence);
    }
    print_sll(temp_node->next);
}


void sortList(Node* node) {  
    Node *current = node, *index = NULL;  
    char temp_letter;
    int temp_occ;  
        
    if(node == NULL) {  
        return;  
    }  
    else {
        while(current != NULL) {  
            index = current->next;  
                
            while(index != NULL) {  
                if(current->occurence > index->occurence) {  
                    temp_letter = current->letter;
                    temp_occ = current->occurence;
                    current->letter = index->letter;
                    current->occurence = index->occurence;
                    index->occurence = temp_occ;
                    index->letter = temp_letter;
                }
                index = index->next;
            }
            current = current->next;
        }
    }
}


Leaf* newNode(char letter, int occurence){ 
    Leaf* temp = (Leaf*)malloc(sizeof(Leaf)); 
    temp->left = temp->right = NULL; 
    temp->letter = letter; 
    temp->occurence = occurence; 
  
    return temp; 
}

Heap* createMinHeap(int capacity){ 
    Heap* MinHeap = (Heap*)malloc(sizeof(Heap)); 
   
    MinHeap->size = 0; 
  
    MinHeap->capacity = capacity; 
  
    MinHeap->array = (Leaf**)malloc(MinHeap->capacity*sizeof(Leaf*)); 
    return MinHeap; 
}

void swapLeaf(Leaf** a, Leaf** b){ 
    Leaf* t = *a; 
    *a = *b; 
    *b = t; 
}

void minSorter(Heap* minHeap, int idx){ 
    int smallest = idx; 
    int left = 2 * idx + 1; 
    int right = 2 * idx + 2; 
  
    if(left < minHeap->size && minHeap->array[left]->occurence < minHeap->array[smallest]->occurence) 
        smallest = left; 
  
    if(right < minHeap->size && minHeap->array[right]->occurence < minHeap->array[smallest]->occurence) 
        smallest = right; 
  
    if(smallest != idx) { 
        swapLeaf(&minHeap->array[smallest], &minHeap->array[idx]);
        minSorter(minHeap, smallest);
    }
}

int isSizeOne(Heap* minHeap){
    return (minHeap->size == 1); 
}

Leaf* extractMin(Heap* minHeap){ 
    Leaf* temp = minHeap->array[0];
    minHeap->array[0] = minHeap->array[minHeap->size - 1]; 
    --minHeap->size; 
    minSorter(minHeap, 0);
    return temp;
}

void insertMinHeap(Heap* minHeap, Leaf* leaf){ 
    ++minHeap->size; 
    int i = minHeap->size - 1; 
  
    while (i && leaf->occurence < minHeap->array[(i - 1) / 2]->occurence) { 
        minHeap->array[i] = minHeap->array[(i - 1) / 2]; 
        i = (i - 1) / 2; 
    } 
    minHeap->array[i] = leaf; 
} 

void buildMinHeap(Heap* minHeap){ 
    int n = minHeap->size - 1; 
    int i; 
  
    for (i = (n - 1) / 2; i >= 0; --i) 
        minSorter(minHeap, i); 
}

void printArray(int arr[], int n){ 
    int i; 
    for (i = 0; i < n; ++i) 
        printf("%d", arr[i]); 
    printf("\n"); 
}

int isLeaf(Leaf* root){ 
    return !(root->left) && !(root->right); 
}

Heap* createAndBuildMinHeap(char data[], int occur[], int size){
    Heap* minHeap = createMinHeap(size); 

    for (int i = 0; i < size; ++i) 
        minHeap->array[i] = newNode(data[i], occur[i]); 
  
    minHeap->size = size; 
    //buildMinHeap(minHeap); 
  
    return minHeap; 
}

Leaf* buildHuffmanTree(char data[], int occur[], int size){ 
    Leaf *left, *right, *top; 
  
    // Step 1: Create a min heap of capacity equal to size.

    Heap* minHeap = createAndBuildMinHeap(data, occur, size);  
    while (!isSizeOne(minHeap)) { 
  
        // Step 2: Extract the two minimum occurence from min heap 
        left = extractMin(minHeap); 
        right = extractMin(minHeap); 
  
        // Step 3:  Create a new internal 
        // node with occuruency equal to the 
        // sum of the two nodes occuruencies. 
        // Make the two extracted node as 
        // left and right children of this new node. 
        // Add this node to the min heap
        // '$' is a special value for internal nodes, not used 
        top = newNode('$', left->occurence + right->occurence); 
  
        top->left = left; 
        top->right = right; 
  
        insertMinHeap(minHeap, top); 
    } 
  
    // Step 4: The remaining node is the root node and the tree is complete. 

    return extractMin(minHeap); 
}

void printCodes(Leaf* root, int arr[], int top){
    if (root->left) { 
  
        arr[top] = 0; 
        printCodes(root->left, arr, top + 1); 
    } 
    if (root->right) { 
        arr[top] = 1; 
        printCodes(root->right, arr, top + 1); 
    } 

    if (isLeaf(root)) { 
  
        printf("%c: ", root->letter); 
        printArray(arr, top); 
        
    } 
}

int treeHeight(Leaf* leaf){
    if(leaf == NULL) return 0;
    return 1 + treeHeight(leaf->left) + treeHeight(leaf->right);
}

void HuffmanCodes(char data[], int occur[], int size){ 
    // Construct Huffman Tree 
    Leaf* root = buildHuffmanTree(data, occur, size); 
    
    int tree_HT = treeHeight(root);

    int arr[tree_HT], top = 0; 
  
    printCodes(root, arr, top);
} 

int listLenght(Node* list){
    if(list == NULL) return 0;
    return 1 + listLenght(list->next);
}

char* fillChar(Node* node){
    
    char* arr = (char*)malloc(listLenght(node)*sizeof(char));

    int i = 0;

    while(node != NULL){
       arr[i] = node->letter;
       i++;
       node = node->next;
   }

    return arr;
}

int* fillOcc(Node* node){
    int* occur= (int*)malloc(listLenght(node)*sizeof(int));

    int i = -1;

   while(node != NULL){
       occur[i] = node->occurence;
       i++;
       node = node->next;
   }

   return occur;
}