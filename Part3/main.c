#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include "prototypes.h"

int main(){
    Node* node = (Node*)malloc(sizeof(Node));
    FILE* file = fopen("Test.txt","r");
    node->letter = '$';

    search(file,node);
    /*printf("List without sort : ");
    print_sll(node);*/

    sortList(node);
    /*printf("Sorted list : ");
    print_sll(node);*/

    fclose(file);
  
    HuffmanCodes(fillChar(node), fillOcc(node), listLenght(node));
    return 0;
}