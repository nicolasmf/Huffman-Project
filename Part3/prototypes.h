typedef struct Node{
    char letter;
    int occurence;
    struct Node* next;
}Node;

Node* addOcc(char c, Node* linked_list);
void search(FILE* file, Node* linked_list); // Create a linked list with letters and their occurences from a text file using addOcc()
void print_sll(Node *node); // Print a Linked List
void sortList(Node* node); // Sort a Linked List from lesser occurences to greater's

typedef struct Leaf{
    char letter;
    int occurence;
    struct Leaf *left, *right;
}Leaf;

typedef struct Heap{
    int size;
    int capacity;
    struct Leaf** array;
}Heap;

Leaf* newNode(char letter, int occurence);
Heap* createMinHeap(int capacity);
void swapLeaf(struct Leaf** a, struct Leaf** b);
void minSorter(Heap* minHeap, int idx);
int isSizeOne(struct Heap* minHeap);
struct Leaf* extractMin(Heap* minHeap);
void insertMinHeap(struct Heap* minHeap, struct Leaf* leaf);
void buildMinHeap(Heap* minHeap);
void printArray(int arr[], int n);
int isLeaf(Leaf* root);
Heap* createAndBuildMinHeap(char data[], int occur[], int size);
Leaf* buildHuffmanTree(char data[], int occur[], int size); // Create Huffman tree from array
void printCodes(Leaf* root, int arr[], int top); // print dictionnary codes
int treeHeight(Leaf* leaf);
void HuffmanCodes(char data[], int occur[], int size); // Get Huffman codes for a dictionnary from simple array using several functions
int listLenght(Node* list);
char* fillChar(Node* node);
int* fillOcc(Node* node);