# Huffman Project

## Checklist 

- [x] Part 1: From letter to bit
- [ ] Part 2: The naive version of the Huffman code
    - [x] 2.1 The occurrences
    - [ ] 2.2 The tree
    - [ ] 2.3 The dictionary
    - [ ] 2.4 Encoding
    - [ ] 2.5 Decoding (optional)
- [ ] Part 3: Optimization
    - [x] 3.1 The occurences
    - [x] 3.2 The tree
    - [ ] 3.3 The dictionary
    - [ ] 3.4 Encoding
    - [ ] 3.5 Decoding
